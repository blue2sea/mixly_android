package com.google.blockly.android.demo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

/**
 * ViewCodeActivity - 2017.5.12
 */
public class ViewCodeActivity extends AppCompatActivity {

    private EditText code_editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_code);
        String sketch = getIntent().getStringExtra("sketch_code");
        initView();
        code_editor.setText(sketch);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blockly_default_actionbar, menu);
        return true;
    }

    private void initView() {
        code_editor = (EditText) findViewById(R.id.code_editor);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // save contents of code_editor to test.ino
        if (id == com.google.blockly.android.R.id.action_save) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            LayoutInflater factory = LayoutInflater.from(this);
            final View saveDlg = factory.inflate(R.layout.file_save_dlg, null);
            final EditText edit_filename = (EditText) saveDlg.findViewById(R.id.filename);
            alert.setTitle("Save File");
            alert.setView(saveDlg);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String ino_filename = edit_filename.getText().toString();
                    save_ino_file(ino_filename);
                    hideSoftKeyboard();
                }

                private void hideSoftKeyboard() {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void save_ino_file(String filename) {
        try {
            File dir = new File("/storage/emulated/0/BlocklyDuino");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File file = new File(dir, filename + ".ino");
            FileOutputStream f_os = new FileOutputStream(file);
            f_os.write(code_editor.getText().toString().getBytes());
            f_os.close();
            Toast.makeText(
                    this,
                    "Save success in BlocklyDuino/" + filename + ".ino",
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(
                    this,
                    "Save failed.",
                    Toast.LENGTH_LONG).show();
        }
    }
}
