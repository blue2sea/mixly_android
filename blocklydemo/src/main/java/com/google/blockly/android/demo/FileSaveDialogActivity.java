package com.google.blockly.android.demo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

/**
 * Custom File Save Dialog Activity - 2017.5.30
 */

public class FileSaveDialogActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.file_save_dlg);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blockly_default_actionbar, menu);
        return true;
    }
}
